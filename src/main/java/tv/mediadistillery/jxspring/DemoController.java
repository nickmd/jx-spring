package tv.mediadistillery.jxspring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DemoController {

    @GetMapping(value = "/hello")
    public String sayHello() {
        return "Hello world, eigth commit!";
    }

}
